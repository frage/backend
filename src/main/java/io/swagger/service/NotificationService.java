package io.swagger.service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import io.swagger.api.NotFoundException;
import io.swagger.api.QuestionApiController;
import io.swagger.model.Discussion;
import io.swagger.model.DiscussionUsers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NotificationService {
    private static final Logger log = LoggerFactory.getLogger(QuestionApiController.class);

    private final DiscussionService discussionService;

    @Autowired
    public NotificationService(DiscussionService discussionService) {
        this.discussionService = discussionService;
    }

    /**
     * Gets the registration tokens of the other user than oppositeUserId
     *
     * @param oppositeUserId The user whose tokens are NOT requested
     * @param discussionId   The corresponding discussion
     * @return List of registration tokens
     */
    public List<String> getRegistrationTokens(UUID oppositeUserId, UUID discussionId) throws NotFoundException, ExecutionException, InterruptedException {
        Discussion discussion = discussionService.getDiscussion(discussionId);
        List<DiscussionUsers> users = discussion.getUsers();
        try {
            return users.stream()
                    .filter(u -> !u.getUserId().equals(oppositeUserId))
                    .findFirst()
                    .orElseThrow()
                    .getFirebaseTokens();
        } catch (NoSuchElementException e) {
            throw new NotFoundException(0, "Couldn't find a discussion with user: " + oppositeUserId.toString());
        }
    }

    public List<String> getRegistrationTokens(io.swagger.model.Message message) throws NotFoundException, ExecutionException, InterruptedException {
        return getRegistrationTokens(message.getUserId(), message.getDiscussionId());
    }

    public List<String> getRegistrationTokens(Discussion discussion) throws ExecutionException {
        try {
            return discussion.getUsers().stream()
                    .flatMap(u -> {
                        try {
                            return getRegistrationTokens(u.getUserId(), discussion.getId()).stream();
                        } catch (NotFoundException | ExecutionException | InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .collect(Collectors.toList());
        } catch (RuntimeException e) {
            throw new ExecutionException(e.getCause());
        }
    }

    public com.google.firebase.messaging.Message.Builder fromMessage(String type, io.swagger.model.Message message) {
        return com.google.firebase.messaging.Message.builder()
                .putData("id", message.getId().toString())
                .putData("type", type)
                .putData("text", message.getText())
                .putData("discussionId", message.getDiscussionId().toString())
                .putData("readAt", message.getReadAt().toString())
                .putData("writtenAt", message.getWrittenAt().toString());
    }

    public com.google.firebase.messaging.Message.Builder fromMessageId(String type, UUID messageId) {
        return com.google.firebase.messaging.Message.builder()
                .putData("id", messageId.toString())
                .putData("type", type);
    }

    public com.google.firebase.messaging.Message.Builder fromDiscussion(String type, Discussion discussion) {
        return com.google.firebase.messaging.Message.builder()
                .putData("id", discussion.getId().toString())
                .putData("type", type)
                .putData("questionId", discussion.getQuestionId().toString());
    }

    public com.google.firebase.messaging.Message.Builder fromDiscussionId(String type, UUID discussionId) {
        return com.google.firebase.messaging.Message.builder()
                .putData("id", discussionId.toString())
                .putData("type", type);
    }

    public void send(List<String> tokens, com.google.firebase.messaging.Message.Builder message) throws ExecutionException {
        Stream<com.google.firebase.messaging.Message> notifications = tokens.stream()
                .map(t -> message.setToken(t).build());
        try {
            notifications.forEach(n -> {
                try {
                    String notificationId = FirebaseMessaging.getInstance().send(n);
                    log.info("Send notification with id: " + notificationId);
                } catch (FirebaseMessagingException e) {
                    log.error("Failed to send notification", e);
                    throw new RuntimeException(e);
                }
            });
        } catch (RuntimeException e) {
            throw new ExecutionException(e.getCause());
        }
    }

    public void send(String type, io.swagger.model.Message message) throws NotFoundException, ExecutionException, InterruptedException {
        send(getRegistrationTokens(message), fromMessage(type, message));
    }

    public void send(String type, Discussion discussion) throws ExecutionException {
        send(getRegistrationTokens(discussion), fromDiscussion(type, discussion));
    }
}

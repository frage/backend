package io.swagger.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import io.swagger.api.NotFoundException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public abstract class AbstractFirebaseService<T> {
    private final String columnName;

    protected AbstractFirebaseService(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    protected abstract T fromMap(Map<String, Object> data);

    protected abstract Map<String, Object> toMap(T object);

    protected CollectionReference getCollection() {
        Firestore firestore = FirestoreClient.getFirestore();
        return firestore.collection(getColumnName());
    }

    protected void addEntity(String entityId, T entity) throws InterruptedException, ExecutionException {
        ApiFuture<WriteResult> result = getCollection()
                .document(entityId)
                .create(toMap(entity));
        result.get();
    }

    protected T deleteEntity(String entityId) throws InterruptedException, ExecutionException, NotFoundException {
        T entity = getEntity(entityId);
        ApiFuture<WriteResult> result = getCollection()
                .document(entityId)
                .delete();
        result.get();
        return entity;
    }

    protected T getEntity(String entityId) throws InterruptedException, ExecutionException, NotFoundException {
        ApiFuture<DocumentSnapshot> result = getCollection()
                .document(entityId)
                .get();
        T obj;
        try {
            obj = this.fromMap(result.get().getData());
        } catch (ClassCastException e) {
            throw new ExecutionException(e);
        }
        if (obj != null) return obj;
        else throw new NotFoundException(0, "Couldn't find entity with ID: " + entityId);
    }

    public List<T> getAllEntities() throws InterruptedException, ExecutionException {
        ApiFuture<QuerySnapshot> result = getCollection().get();
        List<QueryDocumentSnapshot> documents = result.get().getDocuments();
        try {
            return documents.stream()
                    .map(d -> this.fromMap(d.getData()))
                    .collect(Collectors.toList());
        } catch (ClassCastException e) {
            throw new ExecutionException(e);
        }
    }

    protected void updateEntity(String entityId, T newEntity) throws InterruptedException, ExecutionException {
        ApiFuture<WriteResult> result = getCollection()
                .document(entityId)
                .set(toMap(newEntity));
        result.get();
    }
}

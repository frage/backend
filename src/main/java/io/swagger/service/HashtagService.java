package io.swagger.service;

import io.swagger.api.NotFoundException;
import io.swagger.model.Discussion;
import io.swagger.model.Hashtag;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class HashtagService extends AbstractFirebaseService<Hashtag> {
    public static final String COL_NAME = "hashtag";

    protected HashtagService() {
        super(COL_NAME);
    }

    public void addHashtag(Hashtag hashtag) throws InterruptedException, ExecutionException {
        addEntity(hashtag.getName(), hashtag);
    }
    
    public Hashtag deleteHashtag(String hashtag) throws InterruptedException, ExecutionException, NotFoundException {
        return deleteEntity(hashtag);
    }
    
    public Hashtag getHashtag(String hashtag) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(hashtag);
    }
    
    public void updateHashtag(String hashtag, Hashtag newHashtag) throws InterruptedException, ExecutionException {
        updateEntity(hashtag, newHashtag);
    }

    @Override
    protected Hashtag fromMap(Map<String, Object> data) {
        Hashtag entity = new Hashtag();
        entity.setName((String) data.get("name"));
        entity.setDescription((String) data.get("description"));
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(Hashtag object) {
        return Map.of(
                "name", object.getName(),
                "description", object.getDescription()
        );
    }
}

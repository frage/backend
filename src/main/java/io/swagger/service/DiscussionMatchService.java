package io.swagger.service;

import io.swagger.model.Answer;
import io.swagger.model.Discussion;
import io.swagger.model.DiscussionUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DiscussionMatchService {
    private final AnswerService answerService;

    private final DiscussionService discussionService;

    @Autowired
    public DiscussionMatchService(AnswerService answerService, DiscussionService discussionService) {
        this.answerService = answerService;
        this.discussionService = discussionService;
    }

    private Stream<Answer> getAnswersToQuestion(UUID questionId) throws ExecutionException, InterruptedException {
        return answerService.getAllEntities().stream()
                .filter(e -> e.getQuestionId().equals(questionId));
    }

    private static List<UUID> getUsersIdsForDiscussion(Discussion discussion) {
        return discussion.getUsers().stream()
                .map(DiscussionUsers::getUserId)
                .collect(Collectors.toList());
    }

    private Optional<Answer> getAnyAnswerToQuestion(UUID questionId) throws ExecutionException, InterruptedException {
        return getAnswersToQuestion(questionId).findAny();
    }

    private Optional<Answer> getAnswerOfUserNotInAnyDiscussion(UUID questionId) throws ExecutionException, InterruptedException {
        Stream<Answer> answersToSameQuestion = getAnswersToQuestion(questionId);
        try {
            return discussionService.getAllEntities().stream()
                    .flatMap(e -> {
                        List<UUID> usersInDiscussion = getUsersIdsForDiscussion(e);
                        return answersToSameQuestion.filter(a -> !usersInDiscussion.contains(a.getUserId()));
                    })
                    .findAny()
                    .or(() -> {
                        try {
                            return getAnyAnswerToQuestion(questionId);
                        } catch (ExecutionException | InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    });
        } catch (RuntimeException e) {
            throw new ExecutionException(e.getCause());
        }
    }

    private Discussion createDiscussion(Answer existingAnswer, Answer newAnswer) {
        DiscussionUsers existingAnswerUser = new DiscussionUsers()
                .userId(existingAnswer.getUserId())
                .answerId(existingAnswer.getId())
                .firebaseTokens(existingAnswer.getFirebaseTokens())
                .left(false);
        DiscussionUsers newlyAnsweredUser = new DiscussionUsers()
                .userId(newAnswer.getUserId())
                .answerId(newAnswer.getId())
                .firebaseTokens(newAnswer.getFirebaseTokens())
                .left(false);
        Discussion discussion = new Discussion()
                .id(UUID.randomUUID())
                .questionId(newAnswer.getQuestionId())
                .addUsersItem(existingAnswerUser)
                .addUsersItem(newlyAnsweredUser);
        try {
            discussionService.addDiscussion(discussion);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        return discussion;
    }

    public Optional<Discussion> tryMatchUserForAnswer(@NotNull Answer answer) throws ExecutionException, InterruptedException {
        Optional<Answer> otherAnswer = getAnswerOfUserNotInAnyDiscussion(answer.getQuestionId());
        try {
            return otherAnswer.map(a -> createDiscussion(a, answer));
        } catch (RuntimeException e) {
            throw new ExecutionException(e.getCause());
        }
    }
}

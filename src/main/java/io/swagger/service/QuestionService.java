package io.swagger.service;

import io.swagger.api.NotFoundException;
import io.swagger.model.Message;
import io.swagger.model.Question;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class QuestionService extends AbstractFirebaseService<Question> {
    public static final String COL_NAME = "question";

    protected QuestionService() {
        super(COL_NAME);
    }

    public void addQuestion(Question question) throws InterruptedException, ExecutionException {
        addEntity(question.getId().toString(), question);
    }
    
    public Question deleteQuestion(UUID questionId) throws InterruptedException, ExecutionException, NotFoundException {
        return deleteEntity(questionId.toString());
    }
    
    public Question getQuestion(UUID questionId) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(questionId.toString());
    }
    
    public void updateQuestion(UUID questionId, Question newQuestion) throws InterruptedException, ExecutionException {
        updateEntity(questionId.toString(), newQuestion);
    }

    @Override
    protected Question fromMap(Map<String, Object> data) {
        Question entity = new Question();
        entity.setId(UUID.fromString((String) data.get("id")));
        entity.setText((String) data.get("text"));
        entity.setHashtags((List<String>) data.get("hashtags"));
        entity.setManagerId(UUID.fromString((String) data.get("manager_id")));
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(Question object) {
        return Map.of(
                "id", object.getId().toString(),
                "text", object.getText(),
                "hashtags", object.getHashtags(),
                "manager_id", object.getManagerId().toString()
        );
    }
}

package io.swagger.service;

import io.swagger.api.NotFoundException;
import io.swagger.model.Discussion;
import io.swagger.model.DiscussionUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class DiscussionService extends AbstractFirebaseService<Discussion> {
    public static final String COL_NAME = "discussion";

    @Lazy
    private final NotificationService notificationService;

    @Autowired
    protected DiscussionService(@Lazy NotificationService notificationService) {
        super(COL_NAME);
        this.notificationService = notificationService;
    }

    public void addDiscussion(Discussion discussion) throws InterruptedException, ExecutionException {
        addEntity(discussion.getId().toString(), discussion);
        notificationService.send("new-discussion", discussion);
    }

    public Discussion deleteDiscussion(UUID discussionId) throws InterruptedException, ExecutionException, NotFoundException {
        notificationService.send(
                notificationService.getRegistrationTokens(getDiscussion(discussionId)),
                notificationService.fromDiscussionId("delete-discussion", discussionId)
        );
        return deleteEntity(discussionId.toString());
    }

    public Discussion getDiscussion(UUID discussionId) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(discussionId.toString());
    }

    public void updateDiscussion(UUID discussionId, Discussion newDiscussion) throws InterruptedException, ExecutionException {
        updateEntity(discussionId.toString(), newDiscussion);
    }

    @Override
    protected Discussion fromMap(Map<String, Object> data) {
        Discussion entity = new Discussion();
        entity.setId(UUID.fromString((String) data.get("id")));
        entity.setQuestionId(UUID.fromString((String) data.get("questionId")));
        for (Map<String, Object> userMap : (List<Map<String, Object>>) data.get("users")) {
            DiscussionUsers userItem = new DiscussionUsers()
                    .userId(UUID.fromString((String) userMap.get("userId")))
                    .answerId(UUID.fromString((String) userMap.get("answerId")))
                    .firebaseTokens((List<String>) userMap.get("firebaseTokens"))
                    .left((boolean) userMap.get("left"));
            entity.addUsersItem(userItem);
        }
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(Discussion object) {
        return Map.of(
                "id", object.getId().toString(),
                "questionId", object.getQuestionId().toString(),
                "users", object.getUsers().stream().map(u -> Map.of(
                        "left", u.isLeft(),
                        "userId", u.getUserId().toString(),
                        "answerId", u.getAnswerId().toString(),
                        "firebaseTokens", u.getFirebaseTokens()
                )).collect(Collectors.toList())
        );
    }
}

package io.swagger.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import io.swagger.api.NotFoundException;
import io.swagger.model.Answer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class AnswerService extends AbstractFirebaseService<Answer> {
    public static final String COL_NAME = "answer";

    protected AnswerService() {
        super(COL_NAME);
    }

    public void addAnswer(Answer answer) throws InterruptedException, ExecutionException {
        addEntity(answer.getId().toString(), answer);
    }

    public Answer deleteAnswer(UUID answerId) throws InterruptedException, ExecutionException, NotFoundException {
        return deleteEntity(answerId.toString());
    }

    public Answer getAnswer(UUID answerId) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(answerId.toString());
    }

    public void updateAnswer(UUID answerId, Answer newAnswer) throws InterruptedException, ExecutionException {
        updateEntity(answerId.toString(), newAnswer);
    }

    @Override
    protected Answer fromMap(Map<String, Object> data) {
        Answer entity = new Answer();
        entity.setId(UUID.fromString((String) data.get("id")));
        entity.setQuestionId(UUID.fromString((String) data.get("questionId")));
        entity.setText((String) data.get("text"));
        entity.setUserId(UUID.fromString((String) data.get("userId")));
        entity.setFirebaseTokens((List<String>) data.get("firebaseTokens"));
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(Answer object) {
        return Map.of(
                "id", object.getId().toString(),
                "questionId", object.getQuestionId().toString(),
                "text", object.getText(),
                "userId", object.getUserId().toString(),
                "firebaseTokens", object.getFirebaseTokens()
        );
    }
}

package io.swagger.service;

import io.swagger.api.NotFoundException;
import io.swagger.model.Message;
import org.springframework.stereotype.Service;
import org.threeten.bp.OffsetDateTime;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class MessageService extends AbstractFirebaseService<Message> {

    public static final String COL_NAME = "message";

    private final NotificationService notificationService;

    protected MessageService(NotificationService notificationService) {
        super(COL_NAME);
        this.notificationService = notificationService;
    }

    private void sendNotifications(String type, UUID messageId) throws NotFoundException, ExecutionException, InterruptedException {
        io.swagger.model.Message message = getMessage(messageId);
        notificationService.send(
                notificationService.getRegistrationTokens(message),
                notificationService.fromMessageId(type, messageId)
        );
    }

    public void addMessage(Message message) throws NotFoundException, InterruptedException, ExecutionException {
        addEntity(message.getId().toString(), message);
        notificationService.send("new-message", message);
    }

    public Message deleteMessage(UUID messageId) throws InterruptedException, ExecutionException, NotFoundException {
        sendNotifications("delete-message", messageId);
        return deleteEntity(messageId.toString());
    }

    public Message getMessage(UUID messageId) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(messageId.toString());
    }

    public void updateMessage(UUID messageId, Message newMessage) throws InterruptedException, ExecutionException, NotFoundException {
        updateEntity(messageId.toString(), newMessage);
        notificationService.send("update-message", newMessage);
    }

    @Override
    protected Message fromMap(Map<String, Object> data) {
        Message entity = new Message();
        entity.setId(UUID.fromString((String) data.get("id")));
        entity.setText((String) data.get("text"));
        entity.setDiscussionId(UUID.fromString((String) data.get("discussionId")));
        entity.setUserId(UUID.fromString((String) data.get("userId")));
        entity.setReadAt(OffsetDateTime.parse((String) data.get("readAt")));
        entity.setWrittenAt(OffsetDateTime.parse((String) data.get("writtenAt")));
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(Message object) {
        return Map.of(
                "id", object.getId().toString(),
                "discussionId", object.getDiscussionId().toString(),
                "text", object.getText(),
                "userId", object.getUserId().toString(),
                "readAt", object.getReadAt().toString(),
                "writtenAt", object.getWrittenAt().toString()
        );
    }
}

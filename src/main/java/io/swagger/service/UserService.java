package io.swagger.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.QuerySnapshot;
import io.swagger.api.NotFoundException;
import io.swagger.model.User;
import org.springframework.stereotype.Service;
import org.threeten.bp.OffsetDateTime;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class UserService extends AbstractFirebaseService<User> {
    public static final String COL_NAME = "user";

    protected UserService() {
        super(COL_NAME);
    }

    public void addUser(User user) throws InterruptedException, ExecutionException {
        addEntity(user.getId().toString(), user);
    }
    
    public User deleteUser(UUID userId) throws InterruptedException, ExecutionException, NotFoundException {
        return deleteEntity(userId.toString());
    }
    
    public User getUserById(UUID userId) throws ExecutionException, InterruptedException, NotFoundException {
        return getEntity(userId.toString());
    }

    public User getUserByEmail(String email) throws ExecutionException, InterruptedException, NotFoundException {
        try {
            ApiFuture<QuerySnapshot> result = getCollection()
                    .whereEqualTo("email", email.toLowerCase())
                    .get();
            Map<String, Object> data = result.get()
                    .getDocuments()
                    .get(0)
                    .getData();
            return fromMap(data);
        } catch (IndexOutOfBoundsException e) {
            throw new NotFoundException(0, "Couldn't find entity with email: " + email);
        }
    }
    
    public void updateUser(UUID userId, User newUser) throws InterruptedException, ExecutionException {
        updateEntity(userId.toString(), newUser);
    }

    @Override
    protected User fromMap(Map<String, Object> data) {
        User entity = new User();
        entity.setId(UUID.fromString((String) data.get("id")));
        entity.setCreatedAt(OffsetDateTime.parse((String) data.get("createdAt")));
        entity.setEmail((String) data.get("email"));
        entity.setFullName((String) data.get("fullName"));
        entity.setUsername((String) data.get("username"));
        return entity;
    }

    @Override
    protected Map<String, Object> toMap(User object) {
        return Map.of(
                "id", object.getId().toString(),
                "createdAt", object.getCreatedAt().toString(),
                "email", object.getEmail().toLowerCase(),
                "fullName", object.getFullName(),
                "username", object.getUsername()
        );
    }
}

package io.swagger.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class ContentTypeRequestWrapper extends HttpServletRequestWrapper {

    public ContentTypeRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getContentType() {
        return "application/json";
    }

    @Override
    public String getHeader(String name) {
        if (name.equalsIgnoreCase("content-type")) {
            String contentType = super.getHeader(name);
            if (contentType != null && !contentType.contains("text/plain")) return contentType;
            else return "application/json";
        }

        return super.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        List<String> headerNames = Collections.list(super.getHeaderNames());
        if (!headerNames.contains("content-type")) {
            headerNames.add("content-type");
            return Collections.enumeration(headerNames);
        }

        return super.getHeaderNames();
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        if (name.equalsIgnoreCase("content-type")) {
            Enumeration<String> contentType = super.getHeaders(name);
            if (contentType != null) return contentType;
            else return Collections.enumeration(Collections.singletonList("application/json"));
        }

        return super.getHeaders(name);
    }
}
package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.UUID;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Message
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-20T09:05:31.022Z[GMT]")


public class Message   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("discussion_id")
  private UUID discussionId = null;

  @JsonProperty("user_id")
  private UUID userId = null;

  @JsonProperty("written_at")
  private OffsetDateTime writtenAt = null;

  @JsonProperty("read_at")
  private OffsetDateTime readAt = null;

  public Message id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Message text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
   **/
  @Schema(example = "Yeah, I think you are right.", required = true, description = "")
      @NotNull

    public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Message discussionId(UUID discussionId) {
    this.discussionId = discussionId;
    return this;
  }

  /**
   * Get discussionId
   * @return discussionId
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getDiscussionId() {
    return discussionId;
  }

  public void setDiscussionId(UUID discussionId) {
    this.discussionId = discussionId;
  }

  public Message userId(UUID userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
   **/
  @Schema(description = "")
  
    @Valid
    public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public Message writtenAt(OffsetDateTime writtenAt) {
    this.writtenAt = writtenAt;
    return this;
  }

  /**
   * Get writtenAt
   * @return writtenAt
   **/
  @Schema(description = "")
  
    @Valid
    public OffsetDateTime getWrittenAt() {
    return writtenAt;
  }

  public void setWrittenAt(OffsetDateTime writtenAt) {
    this.writtenAt = writtenAt;
  }

  public Message readAt(OffsetDateTime readAt) {
    this.readAt = readAt;
    return this;
  }

  /**
   * Get readAt
   * @return readAt
   **/
  @Schema(description = "")
  
    @Valid
    public OffsetDateTime getReadAt() {
    return readAt;
  }

  public void setReadAt(OffsetDateTime readAt) {
    this.readAt = readAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Message message = (Message) o;
    return Objects.equals(this.id, message.id) &&
        Objects.equals(this.text, message.text) &&
        Objects.equals(this.discussionId, message.discussionId) &&
        Objects.equals(this.userId, message.userId) &&
        Objects.equals(this.writtenAt, message.writtenAt) &&
        Objects.equals(this.readAt, message.readAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, text, discussionId, userId, writtenAt, readAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Message {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    discussionId: ").append(toIndentedString(discussionId)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    writtenAt: ").append(toIndentedString(writtenAt)).append("\n");
    sb.append("    readAt: ").append(toIndentedString(readAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

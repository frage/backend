package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Answer
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-22T16:15:00.842Z[GMT]")


public class Answer   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("question_id")
  private UUID questionId = null;

  @JsonProperty("user_id")
  private UUID userId = null;

  @JsonProperty("firebase_tokens")
  @Valid
  private List<String> firebaseTokens = null;

  public Answer id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Answer text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
   **/
  @Schema(example = "To my mind, the answer is 42.", required = true, description = "")
      @NotNull

    public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Answer questionId(UUID questionId) {
    this.questionId = questionId;
    return this;
  }

  /**
   * Get questionId
   * @return questionId
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getQuestionId() {
    return questionId;
  }

  public void setQuestionId(UUID questionId) {
    this.questionId = questionId;
  }

  public Answer userId(UUID userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public Answer firebaseTokens(List<String> firebaseTokens) {
    this.firebaseTokens = firebaseTokens;
    return this;
  }

  public Answer addFirebaseTokensItem(String firebaseTokensItem) {
    if (this.firebaseTokens == null) {
      this.firebaseTokens = new ArrayList<String>();
    }
    this.firebaseTokens.add(firebaseTokensItem);
    return this;
  }

  /**
   * Should contain one Firebase FCM registration token for each device to which the notifications should be sent
   * @return firebaseTokens
   **/
  @Schema(description = "Should contain one Firebase FCM registration token for each device to which the notifications should be sent")
  
    public List<String> getFirebaseTokens() {
    return firebaseTokens;
  }

  public void setFirebaseTokens(List<String> firebaseTokens) {
    this.firebaseTokens = firebaseTokens;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Answer answer = (Answer) o;
    return Objects.equals(this.id, answer.id) &&
        Objects.equals(this.text, answer.text) &&
        Objects.equals(this.questionId, answer.questionId) &&
        Objects.equals(this.userId, answer.userId) &&
        Objects.equals(this.firebaseTokens, answer.firebaseTokens);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, text, questionId, userId, firebaseTokens);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Answer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    questionId: ").append(toIndentedString(questionId)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    firebaseTokens: ").append(toIndentedString(firebaseTokens)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

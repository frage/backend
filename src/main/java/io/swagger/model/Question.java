package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Question
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-15T17:47:24.237Z[GMT]")


public class Question   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("hashtags")
  @Valid
  private List<String> hashtags = null;

  @JsonProperty("manager_id")
  private UUID managerId = null;

  public Question id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Question text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
   **/
  @Schema(example = "What means living for you?", required = true, description = "")
      @NotNull

    public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Question hashtags(List<String> hashtags) {
    this.hashtags = hashtags;
    return this;
  }

  public Question addHashtagsItem(String hashtagsItem) {
    if (this.hashtags == null) {
      this.hashtags = new ArrayList<String>();
    }
    this.hashtags.add(hashtagsItem);
    return this;
  }

  /**
   * Get hashtags
   * @return hashtags
   **/
  @Schema(example = "[\"life\",\"philosophy\"]", description = "")
  
    public List<String> getHashtags() {
    return hashtags;
  }

  public void setHashtags(List<String> hashtags) {
    this.hashtags = hashtags;
  }

  public Question managerId(UUID managerId) {
    this.managerId = managerId;
    return this;
  }

  /**
   * The user id who created and manages this question
   * @return managerId
   **/
  @Schema(description = "The user id who created and manages this question")
  
    @Valid
    public UUID getManagerId() {
    return managerId;
  }

  public void setManagerId(UUID managerId) {
    this.managerId = managerId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Question question = (Question) o;
    return Objects.equals(this.id, question.id) &&
        Objects.equals(this.text, question.text) &&
        Objects.equals(this.hashtags, question.hashtags) &&
        Objects.equals(this.managerId, question.managerId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, text, hashtags, managerId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Question {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    hashtags: ").append(toIndentedString(hashtags)).append("\n");
    sb.append("    managerId: ").append(toIndentedString(managerId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.DiscussionUsers;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Discussion
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-22T14:59:26.130Z[GMT]")


public class Discussion   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("question_id")
  private UUID questionId = null;

  @JsonProperty("users")
  @Valid
  private List<DiscussionUsers> users = null;

  public Discussion id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Discussion questionId(UUID questionId) {
    this.questionId = questionId;
    return this;
  }

  /**
   * Get questionId
   * @return questionId
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public UUID getQuestionId() {
    return questionId;
  }

  public void setQuestionId(UUID questionId) {
    this.questionId = questionId;
  }

  public Discussion users(List<DiscussionUsers> users) {
    this.users = users;
    return this;
  }

  public Discussion addUsersItem(DiscussionUsers usersItem) {
    if (this.users == null) {
      this.users = new ArrayList<DiscussionUsers>();
    }
    this.users.add(usersItem);
    return this;
  }

  /**
   * Should be an array with two elements representing the two users who have this discussion.
   * @return users
   **/
  @Schema(description = "Should be an array with two elements representing the two users who have this discussion.")
      @Valid
    public List<DiscussionUsers> getUsers() {
    return users;
  }

  public void setUsers(List<DiscussionUsers> users) {
    this.users = users;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Discussion discussion = (Discussion) o;
    return Objects.equals(this.id, discussion.id) &&
        Objects.equals(this.questionId, discussion.questionId) &&
        Objects.equals(this.users, discussion.users);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, questionId, users);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Discussion {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    questionId: ").append(toIndentedString(questionId)).append("\n");
    sb.append("    users: ").append(toIndentedString(users)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

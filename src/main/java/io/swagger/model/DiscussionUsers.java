package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DiscussionUsers
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-22T14:59:26.130Z[GMT]")


public class DiscussionUsers   {
  @JsonProperty("user_id")
  private UUID userId = null;

  @JsonProperty("answer_id")
  private UUID answerId = null;

  @JsonProperty("firebase_tokens")
  @Valid
  private List<String> firebaseTokens = null;

  @JsonProperty("left")
  private Boolean left = null;

  public DiscussionUsers userId(UUID userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
   **/
  @Schema(description = "")
  
    @Valid
    public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public DiscussionUsers answerId(UUID answerId) {
    this.answerId = answerId;
    return this;
  }

  /**
   * Get answerId
   * @return answerId
   **/
  @Schema(description = "")
  
    @Valid
    public UUID getAnswerId() {
    return answerId;
  }

  public void setAnswerId(UUID answerId) {
    this.answerId = answerId;
  }

  public DiscussionUsers firebaseTokens(List<String> firebaseTokens) {
    this.firebaseTokens = firebaseTokens;
    return this;
  }

  public DiscussionUsers addFirebaseTokensItem(String firebaseTokensItem) {
    if (this.firebaseTokens == null) {
      this.firebaseTokens = new ArrayList<String>();
    }
    this.firebaseTokens.add(firebaseTokensItem);
    return this;
  }

  /**
   * Should contain one Firebase FCM registration token for each device to which the notifications should be sent
   * @return firebaseTokens
   **/
  @Schema(description = "Should contain one Firebase FCM registration token for each device to which the notifications should be sent")
  
    public List<String> getFirebaseTokens() {
    return firebaseTokens;
  }

  public void setFirebaseTokens(List<String> firebaseTokens) {
    this.firebaseTokens = firebaseTokens;
  }

  public DiscussionUsers left(Boolean left) {
    this.left = left;
    return this;
  }

  /**
   * Should be true if this user decided to leave the discussion
   * @return left
   **/
  @Schema(description = "Should be true if this user decided to leave the discussion")
  
    public Boolean isLeft() {
    return left;
  }

  public void setLeft(Boolean left) {
    this.left = left;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DiscussionUsers discussionUsers = (DiscussionUsers) o;
    return Objects.equals(this.userId, discussionUsers.userId) &&
        Objects.equals(this.answerId, discussionUsers.answerId) &&
        Objects.equals(this.firebaseTokens, discussionUsers.firebaseTokens) &&
        Objects.equals(this.left, discussionUsers.left);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, answerId, firebaseTokens, left);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DiscussionUsers {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    answerId: ").append(toIndentedString(answerId)).append("\n");
    sb.append("    firebaseTokens: ").append(toIndentedString(firebaseTokens)).append("\n");
    sb.append("    left: ").append(toIndentedString(left)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

package io.swagger.api;

import io.swagger.model.Answer;
import io.swagger.model.Discussion;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.DiscussionService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class DiscussionApiController implements DiscussionApi {

    private static final Logger log = LoggerFactory.getLogger(DiscussionApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final DiscussionService service;

    @org.springframework.beans.factory.annotation.Autowired
    public DiscussionApiController(ObjectMapper objectMapper, HttpServletRequest request, DiscussionService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<Void> addDiscussion(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Discussion body) {
        String accept = request.getHeader("Accept");
        try {
            service.addDiscussion(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Discussion> deleteDiscussion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the discussion id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "discussionId", required = true) String discussionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Discussion oldDiscussion = service.deleteDiscussion(UUID.fromString(discussionId));
                return new ResponseEntity<Discussion>(oldDiscussion, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Discussion>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find discussion with id: " + discussionId.toString());
                return new ResponseEntity<Discussion>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Discussion>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Discussion> getDiscussion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the discussion id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "discussionId", required = true) String discussionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Discussion discussion = service.getDiscussion(UUID.fromString(discussionId));
                return new ResponseEntity<Discussion>(discussion, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Discussion>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find discussion with id: " + discussionId.toString());
                return new ResponseEntity<Discussion>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Discussion>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateDiscussion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the discussion id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "discussionId", required = true) String discussionId,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Discussion body) {
        String accept = request.getHeader("Accept");
        try {
            service.updateDiscussion(UUID.fromString(discussionId), body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}

package io.swagger.api;

import io.swagger.model.Answer;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.Discussion;
import io.swagger.service.AnswerService;
import io.swagger.service.DiscussionMatchService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class AnswerApiController implements AnswerApi {

    private static final Logger log = LoggerFactory.getLogger(AnswerApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final AnswerService answerService;

    private final DiscussionMatchService matchService;

    @org.springframework.beans.factory.annotation.Autowired
    public AnswerApiController(ObjectMapper objectMapper, HttpServletRequest request, AnswerService answerService, DiscussionMatchService matchService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.answerService = answerService;
        this.matchService = matchService;
    }

    public ResponseEntity<Discussion> addAnswer(@Parameter(in = ParameterIn.DEFAULT, description = "", schema = @Schema()) @Valid @RequestBody Answer body) {
        String accept = request.getHeader("Accept");
        try {
            answerService.addAnswer(body);
            Optional<Discussion> matchedDiscussion = matchService.tryMatchUserForAnswer(body);
            if (matchedDiscussion.isPresent()) {
                return new ResponseEntity<Discussion>(matchedDiscussion.get(), HttpStatus.OK);
            } else {
                log.info("Couldn't match with other user answering question with id: " + body.getQuestionId().toString());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (InterruptedException | ExecutionException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Discussion>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public ResponseEntity<Answer> deleteAnswer(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the answer id", required = true, schema = @Schema()) @Valid @RequestParam(value = "answerId", required = true) String answerId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Answer oldAnswer = answerService.deleteAnswer(UUID.fromString(answerId));
                return new ResponseEntity<Answer>(oldAnswer, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Answer>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
                log.info("Couldn't find answer with id: " + answerId.toString());
                return new ResponseEntity<Answer>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Answer>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Answer> getAnswer(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the answer id", required = true, schema = @Schema()) @Valid @RequestParam(value = "answerId", required = true) String answerId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Answer answer = answerService.getAnswer(UUID.fromString(answerId));
                return new ResponseEntity<Answer>(answer, HttpStatus.OK);
            } catch (ExecutionException | InterruptedException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Answer>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
                log.info("Couldn't find answer with id: " + answerId.toString());
                return new ResponseEntity<Answer>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Answer>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<Void> updateAnswer(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the answer id", required = true, schema = @Schema()) @Valid @RequestParam(value = "answerId", required = true) String answerId, @Parameter(in = ParameterIn.DEFAULT, description = "", schema = @Schema()) @Valid @RequestBody Answer body) {
        String accept = request.getHeader("Accept");
        try {
            answerService.updateAnswer(UUID.fromString(answerId), body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}

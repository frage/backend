package io.swagger.api;

import io.swagger.model.Hashtag;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.HashtagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-13T16:54:27.774Z[GMT]")
@RestController
public class HashtagsApiController implements HashtagsApi {

    private static final Logger log = LoggerFactory.getLogger(HashtagsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final HashtagService service;

    @org.springframework.beans.factory.annotation.Autowired
    public HashtagsApiController(ObjectMapper objectMapper, HttpServletRequest request, HashtagService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<List<Hashtag>> getHashtags() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                List<Hashtag> hashtagDescs = service.getAllEntities();
                if (!hashtagDescs.isEmpty()) return new ResponseEntity<List<Hashtag>>(hashtagDescs, HttpStatus.OK);
                else return new ResponseEntity<List<Hashtag>>(HttpStatus.NOT_FOUND);
            } catch (ExecutionException | InterruptedException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Hashtag>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<List<Hashtag>>(HttpStatus.NOT_IMPLEMENTED);
        }
    }

}

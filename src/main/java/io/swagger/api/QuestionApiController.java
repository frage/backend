package io.swagger.api;

import io.swagger.model.Answer;
import io.swagger.model.Message;
import io.swagger.model.Question;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.QuestionService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class QuestionApiController implements QuestionApi {

    private static final Logger log = LoggerFactory.getLogger(QuestionApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final QuestionService service;

    @org.springframework.beans.factory.annotation.Autowired
    public QuestionApiController(ObjectMapper objectMapper, HttpServletRequest request, QuestionService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<Void> addQuestion(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Question body) {
        String accept = request.getHeader("Accept");
        try {
            service.addQuestion(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Question> deleteQuestion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the question id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "questionId", required = true) String questionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Question oldQuestion = service.deleteQuestion(UUID.fromString(questionId));
                return new ResponseEntity<Question>(oldQuestion, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Question>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find question with id: " + questionId.toString());
                return new ResponseEntity<Question>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Question>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Question> getQuestion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the question id" ,required=false,schema=@Schema()) @Valid @RequestParam(value = "questionId", required = true) String questionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Question oldQuestion = service.getQuestion(UUID.fromString(questionId));
                return new ResponseEntity<Question>(oldQuestion, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Question>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find question with id: " + questionId.toString());
                return new ResponseEntity<Question>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Question>(HttpStatus.NOT_IMPLEMENTED);
        }
    }

    public ResponseEntity<Void> updateQuestion(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the question id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "questionId", required = true) String questionId,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Question body) {
        String accept = request.getHeader("Accept");
        try {
            service.updateQuestion(UUID.fromString(questionId), body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}

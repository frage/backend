package io.swagger.api;

import io.swagger.model.Question;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.QuestionService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-13T16:54:27.774Z[GMT]")
@RestController
public class QuestionsApiController implements QuestionsApi {

    private static final Logger log = LoggerFactory.getLogger(QuestionsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final QuestionService service;

    @org.springframework.beans.factory.annotation.Autowired
    public QuestionsApiController(ObjectMapper objectMapper, HttpServletRequest request, QuestionService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    private ResponseEntity<List<Question>> getAllQuestions() {
        try {
            List<Question> questions = service.getAllEntities();
            log.debug("Got following questions: " + questions);
            if (!questions.isEmpty()) return new ResponseEntity<List<Question>>(questions, HttpStatus.OK);
            else return new ResponseEntity<List<Question>>(HttpStatus.NOT_FOUND);
        } catch (ExecutionException | InterruptedException e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<List<Question>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity<List<Question>> filterQuestionsBy(@NotNull List<String> hashtags, UUID userId) {
        log.debug("Filtering by hashtags: " + hashtags + " and user id: " + userId);
        try {
            Stream<Question> questionsStream = service.getAllEntities().stream();
            if (!hashtags.isEmpty()) {
                questionsStream = questionsStream.filter(e -> e.getHashtags().stream().anyMatch(hashtags::contains));
            }
            if (userId != null) {
                questionsStream = questionsStream.filter(e -> e.getManagerId().equals(userId));
            }

            List<Question> questions = questionsStream.collect(Collectors.toList());
            log.debug("Got following questions: " + questions);
            if (!questions.isEmpty()) return new ResponseEntity<List<Question>>(questions, HttpStatus.OK);
            else return new ResponseEntity<List<Question>>(HttpStatus.NOT_FOUND);
        } catch (ExecutionException | InterruptedException e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<List<Question>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Question>> getQuestions(@Parameter(in = ParameterIn.QUERY, description = "Filter questions by these hashtags" ,schema=@Schema()) @Valid @RequestParam(value = "hashtag", required = false) List<String> hashtag, @Parameter(in = ParameterIn.QUERY, description = "Filter questions by this user id" ,schema=@Schema()) @Valid @RequestParam(value = "userId", required = false) UUID userId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if ((hashtag == null || hashtag.isEmpty()) && userId == null) {
                return getAllQuestions();
            } else {
                List<String> hashtags;
                if (hashtag == null) {
                    hashtags = List.of();
                } else {
                    hashtags = hashtag;
                }

                return filterQuestionsBy(hashtags, userId);
            }
        } else {
            return new ResponseEntity<List<Question>>(HttpStatus.NOT_IMPLEMENTED);
        }
    }
}

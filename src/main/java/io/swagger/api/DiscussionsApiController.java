package io.swagger.api;

import io.swagger.model.Discussion;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.Question;
import io.swagger.service.DiscussionService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-20T09:41:47.860Z[GMT]")
@RestController
public class DiscussionsApiController implements DiscussionsApi {

    private static final Logger log = LoggerFactory.getLogger(DiscussionsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final DiscussionService service;

    @org.springframework.beans.factory.annotation.Autowired
    public DiscussionsApiController(ObjectMapper objectMapper, HttpServletRequest request, DiscussionService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<List<Discussion>> getDiscussions(@Parameter(in = ParameterIn.QUERY, description = "Provide the user id", schema = @Schema()) @Valid @RequestParam(value = "userId", required = false) UUID userId, @Parameter(in = ParameterIn.QUERY, description = "Provide the question id", schema = @Schema()) @Valid @RequestParam(value = "questionId", required = false) UUID questionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                List<Discussion> discussions = service.getAllEntities().stream()
                        .filter(e -> filterDiscussion(e, userId, questionId))
                        .collect(Collectors.toList());
                log.debug("Got following discussions: " + discussions);
                if (!discussions.isEmpty()) return new ResponseEntity<List<Discussion>>(discussions, HttpStatus.OK);
                else return new ResponseEntity<List<Discussion>>(HttpStatus.NOT_FOUND);
            } catch (ExecutionException | InterruptedException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Discussion>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Discussion>>(HttpStatus.NOT_IMPLEMENTED);
    }

    private static boolean filterDiscussion(Discussion e, UUID userId, UUID questionId) {
        boolean matchesUserId = userId != null
                ? (e.getUsers().stream()
                .map(u -> u.getUserId())
                .anyMatch(id -> id.equals(userId)))
                : true;
        boolean matchesQuestionId = questionId != null
                ? e.getQuestionId().equals(questionId)
                : true;
        return matchesUserId && matchesQuestionId;
    }
}

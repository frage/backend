package io.swagger.api;

import io.swagger.model.Hashtag;
import io.swagger.model.Message;
import java.util.UUID;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.MessageService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-07-22T14:31:04.098Z[GMT]")
@RestController
public class MessagesApiController implements MessagesApi {

    private static final Logger log = LoggerFactory.getLogger(MessagesApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final MessageService service;

    @org.springframework.beans.factory.annotation.Autowired
    public MessagesApiController(ObjectMapper objectMapper, HttpServletRequest request, MessageService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<List<Message>> getMessages(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the question id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "questionId", required = true) UUID questionId,@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the question id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "discussionId", required = true) UUID discussionId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                List<Message> messages = service.getAllEntities().stream()
                        .filter(m -> m.getDiscussionId().equals(discussionId))
                        .collect(Collectors.toList());
                if (!messages.isEmpty()) return new ResponseEntity<List<Message>>(messages, HttpStatus.OK);
                else return new ResponseEntity<List<Message>>(HttpStatus.NOT_FOUND);
            } catch (ExecutionException | InterruptedException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Message>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Message>>(HttpStatus.NOT_IMPLEMENTED);
    }

}

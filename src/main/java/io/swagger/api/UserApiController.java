package io.swagger.api;

import io.swagger.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.UserService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class UserApiController implements UserApi {

    private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final UserService service;

    @org.springframework.beans.factory.annotation.Autowired
    public UserApiController(ObjectMapper objectMapper, HttpServletRequest request, UserService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<Void> addUser(@Parameter(in = ParameterIn.DEFAULT, description = "", schema = @Schema()) @Valid @RequestBody User body) {
        String accept = request.getHeader("Accept");
        try {
            service.addUser(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<User> deleteUser(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the user id", required = true, schema = @Schema()) @Valid @RequestParam(value = "userId", required = true) String userId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = service.deleteUser(UUID.fromString(userId));
                return new ResponseEntity<User>(user, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
                log.info("Couldn't find user with id: " + userId.toString());
                return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<User>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<User> getUser(@Parameter(in = ParameterIn.QUERY, description = "Provide the user id", required = false, schema = @Schema()) @Valid @RequestParam(value = "userId", required = false) UUID userId, @Parameter(in = ParameterIn.QUERY, description = "Provide the user email address", required = false, schema = @Schema()) @Valid @RequestParam(value = "email", required = false) String email) {
        if (userId != null) return getUserById(userId);
        else if (email != null) return getUserByEmail(email);
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<User> getUserByEmail(@NotNull String email) {
        try {
            User user = service.getUserByEmail(email);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (InterruptedException | ExecutionException e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NotFoundException e) {
            log.info("Couldn't find user with id: " + email);
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    private ResponseEntity<User> getUserById(@NotNull UUID userId) {
        try {
            User user = service.getUserById(userId);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        } catch (InterruptedException | ExecutionException e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NotFoundException e) {
            log.info("Couldn't find user with id: " + userId.toString());
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Void> updateUser(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the user id", required = true, schema = @Schema()) @Valid @RequestParam(value = "userId", required = true) String userId, @Parameter(in = ParameterIn.DEFAULT, description = "", schema = @Schema()) @Valid @RequestBody User body) {
        String accept = request.getHeader("Accept");
        try {
            service.updateUser(UUID.fromString(userId), body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}

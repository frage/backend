package io.swagger.api;

import io.swagger.model.Answer;
import io.swagger.model.Discussion;
import io.swagger.model.Hashtag;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.HashtagService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class HashtagApiController implements HashtagApi {

    private static final Logger log = LoggerFactory.getLogger(HashtagApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final HashtagService service;

    @org.springframework.beans.factory.annotation.Autowired
    public HashtagApiController(ObjectMapper objectMapper, HttpServletRequest request, HashtagService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<Void> addHashtag(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Hashtag body) {
        String accept = request.getHeader("Accept");
        try {
            service.addHashtag(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Hashtag> deleteHashtag(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the hashtag" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "hashtag", required = true) String hashtag) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Hashtag oldHashtag = service.deleteHashtag(hashtag);
                return new ResponseEntity<Hashtag>(oldHashtag, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Hashtag>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find hashtag: " + hashtag);
                return new ResponseEntity<Hashtag>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Hashtag>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Hashtag> getHashtag(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the hashtag" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "hashtag", required = true) String hashtag) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Hashtag hashtagDesc = service.getHashtag(hashtag);
                if (hashtagDesc != null) return new ResponseEntity<Hashtag>(hashtagDesc, HttpStatus.OK);
                else return new ResponseEntity<Hashtag>(HttpStatus.NOT_FOUND);
            } catch (ExecutionException | InterruptedException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Hashtag>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find hashtag: " + hashtag);
                return new ResponseEntity<Hashtag>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Hashtag>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateHashtag(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the hashtag" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "hashtag", required = true) String hashtag,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Hashtag body) {
        String accept = request.getHeader("Accept");
        try {
            service.updateHashtag(hashtag, body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException e) {
            log.error("Couldn't add hashtag to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}

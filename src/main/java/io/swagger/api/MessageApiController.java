package io.swagger.api;

import io.swagger.model.Answer;
import io.swagger.model.Hashtag;
import io.swagger.model.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.service.MessageService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-06-06T17:59:17.094Z[GMT]")
@RestController
public class MessageApiController implements MessageApi {

    private static final Logger log = LoggerFactory.getLogger(MessageApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final MessageService service;

    @org.springframework.beans.factory.annotation.Autowired
    public MessageApiController(ObjectMapper objectMapper, HttpServletRequest request, MessageService service) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.service = service;
    }

    public ResponseEntity<Void> addMessage(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Message body) {
        String accept = request.getHeader("Accept");
        try {
            service.addMessage(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException | NotFoundException e) {
            log.error("Couldn't add message to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Message> deleteMessage(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the message id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "messageId", required = true) String messageId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Message oldMessage = service.deleteMessage(UUID.fromString(messageId));
                return new ResponseEntity<Message>(oldMessage, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Message>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find message with id: " + messageId.toString());
                return new ResponseEntity<Message>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Message>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Message> getMessage(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the message id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "messageId", required = true) String messageId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Message oldMessage = service.getMessage(UUID.fromString(messageId));
                return new ResponseEntity<Message>(oldMessage, HttpStatus.OK);
            } catch (InterruptedException | ExecutionException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Message>(HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (NotFoundException e) {
  log.info("Couldn't find message with id: " + messageId.toString());
                return new ResponseEntity<Message>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Message>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateMessage(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Provide the message id" ,required=true,schema=@Schema()) @Valid @RequestParam(value = "messageId", required = true) String messageId,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody Message body) {
        String accept = request.getHeader("Accept");
        try {
            service.updateMessage(UUID.fromString(messageId), body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (ExecutionException | NotFoundException e) {
            log.error("Couldn't update message to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        } catch (InterruptedException e) {
            log.error("Couldn't wait for call to Firebase", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

}
